Role Name
=========

A role that installs the Prometheus alert manager, <https://github.com/prometheus/alertmanager>

Role Variables
--------------

The most important variables are listed below:

``` yaml
prometheus_alert_m_install: true
prometheus_alert_m_version: 0.25.0
# https://github.com/prometheus/alertmanager/releases/download/v0.24.0/alertmanager-0.24.0.linux-amd64.tar.gz
prometheus_alert_m_download_url: 'https://github.com/prometheus/alertmanager/releases/download/v{{ prometheus_alert_m_version }}/{{ prometheus_alert_m_file }}'
prometheus_alert_m_port: 9093
prometheus_alert_m_behind_proxy: false
prometheus_alert_m_ext_url: "https://localhost"
prometheus_alert_m_loglevel: info
prometheus_alert_m_opts: "--log.level={{ prometheus_alert_m_loglevel }} --config.file={{ prometheus_alert_m_conf_file }} --storage.path={{ prometheus_alert_m_data_dir }} {{ prometheus_alert_m_cluster_opts }}"
# List the additional options here
prometheus_alert_m_additional_opts: ''
prometheus_alert_m_conf_dir: "/opt/prometheus/alertmanager/conf"
prometheus_alert_m_conf_file: "{{ prometheus_alert_m_conf_dir }}/alertmanager.yml"
prometheus_alert_m_data_dir: "/opt/prometheus/alertmanager/data"
prometheus_alert_m_firewalld_rules: 'enabled'
prometheus_alert_m_firewalld_ports:
  - port: '{{ prometheus_alert_m_port }}'
    protocol: 'tcp'
    state: '{{ prometheus_alert_m_firewalld_rules }}'
    zone: '{{ firewalld_default_zone }}'

prometheus_alert_m_install_conf: false
prometheus_alert_m_smtp_smarthost: "localhost:25"
prometheus_alert_m_smtp_from: "alerts@localhost"
prometheus_alert_m_smtp_authenticated: false

prometheus_alert_m_default_receiver: "global-alerts"
prometheus_alert_m_alerts_group_by: "['alertname', 'cluster']"

prometheus_alert_m_cluster_enabled: false
prometheus_alert_m_cluster_port: 9094
prometheus_alert_m_cluster_addr: 0.0.0.0
prometheus_alert_m_cluster_peers:
  - "localhost:{{ prometheus_alert_m_cluster_port }}"
prometheus_alert_m_cluster_opts: "{% if not prometheus_alert_m_cluster_enabled %} --cluster.listen-address ''{% else %} --cluster.listen-address '{{ prometheus_alert_m_cluster_addr }}:{{ prometheus_alert_m_cluster_port }}'{% for peer in prometheus_alert_m_cluster_peers %} --cluster.peer {{ peer }}{% endfor %}{% endif %}"
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
